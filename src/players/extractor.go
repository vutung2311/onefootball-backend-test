package players

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"
)

type Extractor struct {
	httpClient        *http.Client
	teamExtractionURL string
}

func NewExtractor(client *http.Client, teamExtractionURL string) *Extractor {
	return &Extractor{
		httpClient:        client,
		teamExtractionURL: teamExtractionURL,
	}
}

func logError(err error) {
	if strings.Contains(err.Error(), "context canceled") {
		return
	}
	log.Println(fmt.Sprintf("ERR: %v", err))
}

type PlayerDTO struct {
	Player
	TeamName string
}

func (e *Extractor) fetchAndIncreaseTeamID(
	ctx context.Context,
	teamIdToCheckChan, foundTeamIdChan chan uint32,
	teamsToCheck map[string]bool,
	dtoChan chan PlayerDTO,
) {
	request, err := http.NewRequest(http.MethodGet, fmt.Sprintf(e.teamExtractionURL, <-teamIdToCheckChan), nil)
	if err != nil {
		logError(err)
		return
	}
	request = request.WithContext(ctx)
	// fake user agent to avoid forbidden
	request.Header.Set("User-Agent", "Golang_Spider_Bot/3.0")
	resp, err := e.httpClient.Do(request)
	if err != nil {
		logError(err)
		return
	}
	if resp.StatusCode != http.StatusOK {
		return
	}
	var response TeamResponse
	err = json.NewDecoder(resp.Body).Decode(&response)
	if err != nil {
		logError(err)
		return
	}
	_ = resp.Body.Close()
	if _, ok := teamsToCheck[response.Data.Team.Name]; !ok {
		return
	}
	for _, player := range response.Data.Team.Players {
		player := player
		select {
		case dtoChan <- PlayerDTO{
			Player:   player,
			TeamName: response.Data.Team.Name,
		}:
		case <-ctx.Done():
			return
		}
	}
	foundTeamIdChan <- response.Data.Team.ID
}

func (e *Extractor) ExtractPlayersFromTeams(teams []string) chan PlayerDTO {
	workerCount := 10
	output := make(chan PlayerDTO)
	foundTeamIdChan, teamIdToCheckChan := make(chan uint32), make(chan uint32, workerCount)
	teamToCheckMap := make(map[string]bool)
	for _, team := range teams {
		team := team
		teamToCheckMap[team] = true
	}

	ctx, cancelFunc := context.WithTimeout(context.Background(), 10*time.Second)
	for i := 0; i < workerCount; i++ {
		go func() {
			for {
				select {
				case <-ctx.Done():
					return
				default:
					e.fetchAndIncreaseTeamID(ctx, teamIdToCheckChan, foundTeamIdChan, teamToCheckMap, output)
				}
			}
		}()
	}
	go func() {
		foundTeamCount := 0
		teamIdToCheck := uint32(0)
		for {
			select {
			case <-foundTeamIdChan:
				foundTeamCount++
			case teamIdToCheckChan <- teamIdToCheck:
				teamIdToCheck++
			}
			if foundTeamCount >= len(teams) {
				break
			}
		}
		cancelFunc()
		close(foundTeamIdChan)
		close(teamIdToCheckChan)
		close(output)
	}()

	return output
}

func AggregatePlayer(input chan PlayerDTO) []*PlayerRecord {
	playerRecordMap := make(map[string]*PlayerRecord)
	for player := range input {
		_, ok := playerRecordMap[player.ID]
		if !ok {
			age, err := strconv.Atoi(player.Age)
			if err != nil {
				logError(err)
			}
			playerRecordMap[player.ID] = &PlayerRecord{
				FullName: player.GetFullName(),
				Age:      uint(age),
				Teams:    []string{player.TeamName},
			}
			continue
		}

		playerRecordMap[player.ID].Teams = append(playerRecordMap[player.ID].Teams, player.TeamName)
	}

	output := make([]*PlayerRecord, 0)
	for _, record := range playerRecordMap {
		output = append(output, record)
	}
	sort.Slice(output, func(i, j int) bool {
		return strings.Compare(output[i].FullName, output[j].FullName) < 0
	})

	return output
}
