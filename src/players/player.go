package players

import (
	"fmt"
	"strings"
)

type Player struct {
	ID           string `json:"id"`
	Country      string `json:"country"`
	FirstName    string `json:"firstName"`
	LastName     string `json:"lastName"`
	Name         string `json:"name"`
	Position     string `json:"position"`
	Number       uint   `json:"number"`
	BirthDate    string `json:"birthDate"`
	Age          string `json:"age"`
	Height       uint   `json:"height"`
	Weight       uint   `json:"weight"`
	ThumbnailSrc string `json:"thumbnailSrc"`
}

func (p Player) GetFullName() string {
	return fmt.Sprintf("%s %s", p.FirstName, p.LastName)
}

type PlayerRecord struct {
	FullName string
	Age      uint
	Teams    []string
}

func (p PlayerRecord) GetAllTeams() string {
	return strings.Join(p.Teams, ", ")
}
