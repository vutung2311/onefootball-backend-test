package players

type TeamResponse struct {
	Status  string `json:"status"`
	Code    uint   `json:"code"`
	Data    Data   `json:"data"`
	Message string `json:"message"`
}

type Data struct {
	Team Team `json:"team"`
}

type Team struct {
	ID           uint32        `json:"id"`
	OptaID       uint32        `json:"optaId"`
	Name         string        `json:"name"`
	LogoURLs     []LogoURL     `json:"logoUrls"`
	IsNational   bool          `json:"isNational"`
	Competitions []Competition `json:"competitions"`
	Officials    []Official    `json:"officials"`
	Players      []Player      `json:"players"`
	Colors       Color         `json:"colors"`
}

type LogoURL struct {
	Size string `json:"size"`
	URL  string `json:"url"`
}

type Competition struct {
	CompetitionID   int    `json:"competitionId"`
	CompetitionName string `json:"competitionName"`
}

type Color struct {
	ShirtColorHome string `json:"shirtColorHome"`
	ShirtColorAway string `json:"shirtColorAway"`
	CrestMainColor string `json:"crestMainColor"`
	MainColor      string `json:"mainColor"`
}

type Official struct {
	CountryName string `json:"countryName"`
	ID          string `json:"id"`
	FirstName   string `json:"firstName"`
	LastName    string `json:"lastName"`
	Country     string `json:"country"`
	Position    string `json:"position"`
}
