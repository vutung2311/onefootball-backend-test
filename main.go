package main

import (
	"fmt"
	"net/http"
	"players"
	"time"
)

func main() {
	httpClient := http.Client{
		Timeout: 10 * time.Second,
	}
	extractor := players.NewExtractor(
		&httpClient,
		"https://vintagemonster.onefootball.com/api/teams/en/%d.json",
	)
	teamList := []string{
		"Germany",
		"England",
		"France",
		"Spain",
		"Manchester United",
		"Arsenal",
		"Chelsea",
		"Barcelona",
		"Real Madrid",
		"Bayern Munich",
	}
	index := 1
	for _, record := range players.AggregatePlayer(extractor.ExtractPlayersFromTeams(teamList)) {
		fmt.Printf("%d. %s; %d; %s\n", index, record.FullName, record.Age, record.GetAllTeams())
		index++
	}
}
