# How to run
## 1. Running in your native environment
* Clone this repository to your local directory
> git clone https://gitlab.com/vutung2311/onefootball-backend-test.git /your/local/directory
* Set GOPATH to `/your/local/directory`
* Run the test
> GOPATH=/your/local/directory go run /your/local/directory/main.go
## 2. Running inside docker
* For this you need to have docker and docker-compose ready on your machine
> docker-compose run app